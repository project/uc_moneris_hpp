<?php
// $Id:$

/**
 * @file
 * An Ubercart module used for processing payments using Moneris.com hosted pay page
 *
 * Based on uc_moneris, uc_paypal, and pg_moneris
 * Developed by Steve McCullough (steve@stevemccullough.ca)
 * Development sponsored by Leatherbeaten (http://leatherbeaten.com)
 */


/**
 * Implementation of hook_menu().
 */
function uc_moneris_hpp_menu() {
  $items = array();
  $items['cart/checkout/uc_moneris_hpp_return'] = array(
    'title' => 'Callback for return from Hosted Payment Page',
    'page callback' => 'uc_moneris_hpp_return',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}


/**
 * Implementation of hook_theme().
 */
function uc_moneris_hpp_theme($existing, $type, $theme, $path) {
  $themes = array();
  
  $themes['uc_moneris_hpp_receipt_page'] = array(
    'arguments' => array('moneris' => NULL, 'order' => NULL),
  );

  return $themes;
}


/**
 * Implementation of hook_payment_method().
 */
;function uc_moneris_hpp_payment_method() {
  $methods = array();

  // add CC logo images to method title
  $path = base_path() . drupal_get_path('module', 'uc_moneris_hpp');
  $title = '';
  $cc_types = array('visa', 'mastercard', 'discover', 'amex', 'sears');
  foreach ($cc_types as $type) {
    $title .= ' <img class="uc-credit-cctype uc-credit-cctype-' . $type . '" src="'. $path .'/images/'. $type .'.gif" alt= "'. $type .'" title ="'. $type .'" />';
  }

  $methods[] = array(
    'id' => 'moneris_hpp',
    'name' => t('Moneris Hosted Pay Page'),
    'title' => $title,
    'review' => t('Credit Card'),
    'desc' => t('Allow users to submit payments using the Moneris Hosted Pay Page.'),
    'callback' => 'uc_moneris_hpp',
    'weight' => 1,
    'checkout' => FALSE,
    'no_gateway' => TRUE,
  );

  return $methods;
}


/*
 *  Handles the HPP payment method
 */
function uc_moneris_hpp($op, &$arg1) {
  switch ($op) {
    case 'settings':
      global $base_url;
      $form['moneris_hpp_settings']['uc_moneris_hpp_info'] = array(
        '#type'  => 'markup',
        '#value' => t('<div><strong>Configuration instructions for the Moneris HPP web interface</strong></div>
        <ul>
        <li>Under "Response method", choose "Sent to your server as a POST"</li>
        <li>set the approved and the declined URLs to: <strong>' . $base_url . '/cart/checkout/uc_moneris_hpp_return</strong></li>
        <li>Under "Security features": turn on verification and use the same URL for "Response URL"; choose "Displayed as XML on our server" as the response method</li>  
        </ul>'),
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_transaction_mode'] = array(
        '#type' => 'select',
        '#title' => t('Transaction mode'),
        '#description' => t('Set to "test" to use Moneris\'s test servers and "production" to actually process payments. Select "Debug" to add on-screen reporting to the test mode.'),
        '#options' => array(
          'production' => t('Production'),
          'test' => t('Test'),
          'debug' => t('Debug'),
        ),
        '#default_value' => variable_get('uc_moneris_hpp_transaction_mode', 'test'),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_country'] = array(
        '#type' => 'select',
        '#title' => t('Moneris country'),
        '#description'=> t('The Moneris pay page differs slightly between the US and Canadian versions.'),
        '#default_value' => variable_get('uc_moneris_hpp_country', 'ca'),
        '#options' => array(
          'ca' => t('Canada'),
          'us' => t('United States'),
        ),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_id'] = array(
        '#type'          => 'textfield',
        '#title'         => t('HPP ID'),
        '#default_value' => variable_get('uc_moneris_hpp_id',''),
        '#description'   => t("Please enter your Moneris hosted pay page ID. <div>(This called <strong>ps_store_id</strong> in the Canadian version, and <strong>hpp_id</strong> in the US version.)</div>"),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_key'] = array(
        '#type'          => 'textfield',
        '#title'         => t('HPP Key'),
        '#default_value' => variable_get('uc_moneris_hpp_key',''),
        '#description'   => t("Please enter your Moneris hosted pay page key."),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_lang'] = array(
        '#type'          => 'select',
        '#title'         => t('Language'),
        '#options'       => array('en-ca' => 'English', 'fr-ca' => ' French'),
        '#default_value' => variable_get('uc_moneris_hpp_lang','en-ca'),
        '#description'   => t("The language for the Hosted Paypage display. (This is only relevant for Canadian Moneris accounts.)"),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_completed_message'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Purchase complete message'),
        '#default_value' => variable_get('uc_moneris_hpp_completed_message', t('Thank you for your order')),
        '#description'   => t("Customize the message that is displayed on successful payment."),
        '#required'      => TRUE
      );
      $form['moneris_hpp_settings']['uc_moneris_hpp_completed_page'] = array(
        '#type'          => 'select',
        '#title'         => t('Purchase complete page'),
        '#options'       => array('order' => 'Order summary', 'custom' => 'Custom summary'),
        '#default_value' => variable_get('uc_moneris_hpp_completed_page','order'),
        '#description'   => t("Where should customers be directed upon successful payment? <div>The <em>Order summary</em> requires them to be authorized users with permission to see their orders. It ddisplays their order with the message specified above, if any.</div><div>The <em>Custom summary</em> page is provided by ec_moneris_hpp, and lists the ordered items and Moneris payment details. It can be customized using the theme_uc_moneris_hpp_receipt_page() theme function.</div>"),
        '#required'      => TRUE
      );

      return $form;
      break;
  }
}


/**
 * Implementation of hook_form_alter
 */
function uc_moneris_hpp_form_alter(&$form, $form_state, $form_id) {  
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    
    // for HPP payment, we add a bunch of hidden fields and send the form to Moneris on submit
    if ($order->payment_method == 'moneris_hpp') {
      
      // variables and URLs differ according to testing mode and country
      $country = variable_get('uc_moneris_hpp_country', 'ca');
      $mode    = variable_get('uc_moneris_hpp_transaction_mode', 'test');
      $debug   = ($mode == 'debug');
      $testing = ($mode == 'test' || $debug);
      
      // if testing, create a fake order ID  to avoid duplicate order number errors @ Moneris
      if ($testing) $order->order_id = 'uc_moneris_hpp_' . $order->order_id;
      
      // save order ID for verification later
      $_SESSION['uc_moneris_hpp_order_id'] = $order->order_id;
      
      // set args for uc currency formatting
      $context = array('revision' => 'formatted-original', 'type' => 'amount');
      $options = array('sign' => FALSE);
      
      // hook_uc_moneris_hpp_alter_country: allow programmatic changes to country 
      uc_moneris_hpp_call_hook('uc_moneris_hpp_alter_country', $country);
      
      // insert hidden values for Moneris HPP
      switch ($country) {
        case 'ca' :
          $subdomain = ($testing) ? 'esqa' : 'www3';
          $url       = "https://$subdomain.moneris.com/HPPDP/index.php";
          $vars      = array(
            'ps_store_id'  => variable_get('uc_moneris_hpp_id',''), 
            'hpp_key'      => variable_get('uc_moneris_hpp_key',''), 
            'lang'         => variable_get('uc_moneris_hpp_lang','en-ca'),
            'charge_total' => uc_price($order->order_total, $context, $options),
            'order_id'     => $order->order_id,
            'bill_first_name'        => $order->billing_first_name,
            'bill_last_name'         => $order->billing_last_name,
            'bill_company_name'      => $order->billing_company,
            'bill_address_one'       => $order->billing_street1,
            'bill_city'              => $order->billing_city,
            'bill_state_or_province' => uc_get_zone_code($order->billing_zone),
            'bill_postal_code'       => $order->billing_postal_code,
            'bill_country'           => uc_country_get_by_id($order->billing_country),
            'ship_first_name'        => $order->delivery_first_name,
            'ship_last_name'         => $order->delivery_last_name,
            'ship_company_name'      => $order->delivery_company,
            'ship_address_one'       => $order->delivery_street1,
            'ship_city'              => $order->delivery_city,
            'ship_state_or_province' => uc_get_zone_code($order->delivery_zone),
            'ship_postal_code'       => $order->delivery_postal_code,
            'ship_country'           => uc_country_get_by_id($order->delivery_country),
          );
          break;
      
        case 'us' :
          $subdomain = ($testing) ? 'esplusqa' : 'esplus';
          $url       = "https://$subdomain.moneris.com/DPHPP/index.php";
          $vars      = array(
            'hpp_id'   => variable_get('uc_moneris_hpp_id',''), 
            'hpp_key'  => variable_get('uc_moneris_hpp_key',''), 
            'amount'   => uc_price($order->order_total, $context, $options),
            'order_no' => $order->order_id,
            'od_bill_firstname' => $order->billing_first_name,
            'od_bill_lastname'  => $order->billing_last_name,
            'od_bill_company'   => $order->billing_company,
            'od_bill_address'   => $order->billing_street1,
            'od_bill_city'      => $order->billing_city,
            'od_bill_state'     => uc_get_zone_code($order->billing_zone),
            'od_bill_zipcode'   => $order->billing_postal_code,
            'od_bill_country'   => uc_country_get_by_id($order->billing_country),
            'od_ship_firstname' => $order->delivery_first_name,
            'od_ship_lastname'  => $order->delivery_last_name,
            'od_ship_company'   => $order->delivery_company,
            'od_ship_address'   => $order->delivery_street1,
            'od_ship_city'      => $order->delivery_city,
            'od_ship_state'     => uc_get_zone_code($order->delivery_zone),
            'od_ship_zipcode'   => $order->delivery_postal_code,
            'od_ship_country'   => uc_country_get_by_id($order->delivery_country),
          );
          break;
      }
  
      // call hook_uc_moneris_hpp_formfields_alter
      uc_moneris_hpp_call_hook('uc_moneris_hpp_formfields_alter', $vars);

      // debugging
      if ($debug) uc_moneris_hpp_debug($vars);
      
      // add HPP vars to form object
      foreach ($vars as $name => $value) {
        $form['uc_moneris_hpp'][$name] = array(
          '#type'  => 'hidden',
          '#value' => $value,
        );
      }
      
      // remove "Back" button because it submits the form to moneris
      // we also remove the instruction to use it in uc_moneris_hpp_uc_get_message_alter
      unset($form['back']);
      
      // change submit destination
      $form['#action'] = $url;
    }
  }
}


// utility implementing hooks with args passed by reference
function uc_moneris_hpp_call_hook($hook, &$vars) {
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    $function($vars);
  }
}


/**
 * menu callback
 * return from hosted payment page
 **/
function uc_moneris_hpp_return() {
  $country = variable_get('uc_moneris_hpp_country', 'ca');
  $mode    = variable_get('uc_moneris_hpp_transaction_mode', 'test');
  $debug   = ($mode == 'debug');
  $testing = ($mode == 'test' || $debug);
  
  // hook_uc_moneris_hpp_alter_country: allow programmatic changes to country 
  uc_moneris_hpp_call_hook('uc_moneris_hpp_alter_country', $country);
  
  // set args for uc currency formatting
  $context = array('revision' => 'formatted-original', 'type' => 'amount');
  $options = array('sign' => FALSE);
  
  // parse important POST values into variables
  // note that some keys are different for ca/us
  $p_vars = array(
    'p_order_id' => array( 'ca' => 'response_order_id', 'us' => 'order_no'),
    'p_message'  => array( 'ca' => 'message', 'us' => 'message'),
    'p_code'     => array( 'ca' => 'response_code', 'us' => 'response_code'),
    'p_tx_key'   => array( 'ca' => 'transactionKey', 'us' => 'verify_key'),
    'p_amount'   => array( 'ca' => 'charge_total', 'us' => 'amount'),
    'p_tx_num'   => array( 'ca' => 'txn_num', 'us' => 'txn_num'),
  );
  foreach ($p_vars as $name => $data) {
    $$name = $_POST[$data[$country]];
  }

  // record approval status
  $approved = (intval($p_code) < 50);
  $watchdog = array(
    '!order_no'      => $p_order_id, 
    '!message'       => $p_message,
    '!response_code' => $p_code,
  );
  watchdog('uc_moneris_hpp', 'Moneris HPP !response_code ("!message") for order !order_no.', $watchdog, WATCHDOG_NOTICE);

  // debug
  if ($debug) uc_moneris_hpp_debug($_POST);
  
  // spoofed order id on approval?
  if ($approved && $_SESSION['uc_moneris_hpp_order_id'] != $p_order_id) uc_moneris_hpp_spoofed_id($watchdog);
  
  // Verify transaction
  if (is_numeric($p_code)) {
    if (!empty($p_tx_key)) {
      switch ($country) {
        case 'ca' :
          $subdomain = ($testing) ? 'esqa' : 'www3';
          $url       = "https://$subdomain.moneris.com/HPPDP/verifyTxn.php?";
          $vars = array(
            'ps_store_id'    => variable_get('uc_moneris_hpp_id',''), 
            'hpp_key'        => variable_get('uc_moneris_hpp_key',''), 
            'transactionKey' => $p_tx_key,
          );  
          break;
        
        case 'us' :
          $subdomain = ($testing) ? 'esplusqa' : 'esplus';
          $url       = "https://$subdomain.moneris.com/DPHPP/index.php?";
          $vars = array(
            'hpp_id'     => variable_get('uc_moneris_hpp_id',''), 
            'hpp_key'    => variable_get('uc_moneris_hpp_key',''), 
            'verify_key' => $p_tx_key,
          );  
          break;
      }
      
      // hook_uc_moneris_hpp_verify_alter: allow programmatic changes to vars
      uc_moneris_hpp_call_hook('uc_moneris_hpp_verify_alter', $vars);
      
      // verify the transaction
      $url       = $url . http_build_query($vars, '', '&');
      $response  = file_get_contents($url);     
      $xml       = simplexml_load_string($response);

      // debug
      if ($debug) {
        uc_moneris_hpp_debug($response);
        uc_moneris_hpp_debug(print_r($xml, true));
      }
  
      // parse XML into variables
      // again, there are some ca/us differences in field names
      $xml_vars = array(
        'verify_order_id' => array( 'ca' => 'order_id', 'us' => 'order_no'),
        'verify_message'  => array( 'ca' => 'status', 'us' => 'message'),
        'verify_code'     => array( 'ca' => 'response_code', 'us' => 'response_code'),
        'verify_tx_key'   => array( 'ca' => 'transactionKey', 'us' => 'verify_key'),
        'verify_amount'   => array( 'ca' => 'amount', 'us' => 'amount'),
      );
      foreach ($xml_vars as $name => $data) {
        $eval = 'if (isset($xml->' . $data[$country] . ')) $' . $name . ' = (string)$xml->' . $data[$country] . ';';
        eval($eval);
      }
      
      // record verification status
      $verified = (strpos($verify_message, 'Valid') === 0);
      $watchdog['!order_no'] = $verify_order_id;
      $watchdog['!message']  = $verify_message;
      $watchdog['!xml']      = $response;
      watchdog('uc_moneris_hpp', 'Verification !message for !order_no : <pre>!xml</pre>', $watchdog, WATCHDOG_NOTICE);
      
      // spoofed order id on verification?
      if ($verified && $_SESSION['uc_moneris_hpp_order_id'] != $verify_order_id) uc_moneris_hpp_spoofed_id($watchdog);
    }
    
    else {
      $watchdog['post'] = $_POST;
      watchdog('uc_moneris_hpp', 'A verification code was not returned for order !order_no : <pre>!post</pre>', $watchdog, WATCHDOG_WARNING);
    }
    
    // we are confident we have the correct order ID
    $order_id = $_SESSION['uc_moneris_hpp_order_id'];
    if ($testing) $order_id = str_replace( 'uc_moneris_hpp_', '', $order_id);
    $order = uc_order_load($order_id);
    unset($_SESSION['uc_moneris_hpp_order_id']);
    
    // were we wrong?
    if (!$order) die('No order');
    
    // success
    if ($approved && ($verified || empty($p_tx_key))) {
      // totals OK? 
      if ($p_amount != $order->order_total) {
        watchdog('uc_moneris_hpp', 'Payment !txn_num for order !order_id did not equal the order total.', array('!txn_num' => $p_tx_num, '!order_id' => $order->order_id), WATCHDOG_WARNING);
      }
      
      // record payment, comments
      $comment = t('Moneris HPP transaction ID: !txn_num', array('!txn_num' => $p_tx_num));
      uc_payment_enter($order->order_id, 'uc_moneris_hpp', $p_amount, $order->uid, NULL, $comment);
      uc_cart_complete_sale($order);
      uc_order_comment_save($order->order_id, 0, t('Payment of @amount submitted through Moneris Hosted Pay Page.', array('@amount' => uc_price($p_amount, $context, $options))), 'order', 'payment_received');
      uc_order_comment_save($order->order_id, 0, t('Moneris HPP reported a payment of @amount.', array('@amount' => uc_price($p_amount, $context, $options))));
      if ($verified) uc_order_comment_save($order->order_id, 0, t('Moneris HPP verified a payment of @amount.', array('@amount' => uc_price($verify_amount, $context, $options))));
      
      // display message, redirect to order 
      $destination = variable_get('uc_moneris_hpp_completed_page', 'order');
      switch ($destination) {
        case 'custom' :
          $output = theme('uc_moneris_hpp_receipt_page', $_POST, $order);
          print theme('page', $output);
          die();
          break;
          
        case 'order' :
          if ($message = variable_get('uc_moneris_hpp_completed_message', NULL)) drupal_set_message($message);
          global $user;
          drupal_goto('user/' . $user->uid . '/order/' . $order->order_id);
          break;
      }
    }
  }
  
  // Fall through here on failure
  drupal_set_message(t('Your payment did not complete successfully. Please try again'), 'error');
  drupal_goto('cart/checkout');
}


function theme_uc_moneris_hpp_receipt_page($moneris = NULL, $order = NULL) {
  $output = '';
  
  if ($message = variable_get('uc_moneris_hpp_completed_message', NULL)) $output .= '<div class="messages">' . t($message) . '</div>';
      
  // order info
  $title = (count($order->products) > 1) ? 'Items' : 'Item';
  $output .= "<h2>$title</h2>\n";
  $array = json_decode(json_encode($order->products), true);
  $output .= theme_uc_cart_block_items($array);
  
  // payment info
  $output .= "<h2>Payment Information</h2>\n";
  $moneris_header = array();
  $moneris_data = array();
  $moneris_data[] = array('Cardholder: ', $moneris['cardholder']);
  $moneris_data[] = array('Card number: ', $moneris['f4l4']);
  $moneris_data[] = array('Amount: ', $moneris['charge_total']);
  $moneris_data[] = array('Transaction Type: ', $moneris['trans_name']);
  $moneris_data[] = array('Date and Time: ', $moneris['date_stamp'] . ' ' . $moneris['time_stamp']);
  $moneris_data[] = array('Authorisation Code : ', $moneris['bank_approval_code']);
  $moneris_data[] = array('Reference Number: ', $moneris['bank_transaction_id']);
  $output .= theme_table($moneris_header, $moneris_data);
  
  $output .= print_r($order, TRUE);
  
  return $output;
}


/**
 * Implements hook_uc_message_alter().
 */
function uc_moneris_hpp_uc_get_message_alter(&$messages) {
  $messages['review_instructions'] = t("Your order is almost complete. Please review the details below and click 'Submit order' if all the information is correct.");
}


/*
 * Utility to handle mismatched order IDs
 * 
 * following the lead of uc_paypal, we provide an ambiguous user message
 */
function uc_moneris_hpp_spoofed_id($watchdog = array()) {
  $watchdog['order_no_orig'] = $_SESSION['cart_order'];
  watchdog('uc_moneris_hpp', 'Possible order ID spoofing: !order_no_orig vs !order_no', $watchdog, WATCHDOG_WARNING);
  drupal_set_message(t('Thank you for your order! We will be notified by Moneris once we have received your payment.'));
  drupal_goto('cart');
}


/*
 * Utility to display debug info
 * 
 * following the lead of uc_paypal, we provide an ambiguous user message
 */
function uc_moneris_hpp_debug($data = array()) {
  if (function_exists('dsm')) {
    dsm($data);
  }
  else {
    drupal_set_message('<pre>' . print_r($data, TRUE) . '</pre>');
  }
  
  watchdog('uc_moneris_hpp_debug', '<pre>' . print_r($data, TRUE) . '</pre>');
}

