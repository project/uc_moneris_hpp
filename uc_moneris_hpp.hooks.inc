<?php

/*
 * Allows modules to set the country code programmatically, so
 * you can use both the USA and Canadian services.
 *
 * @param $country
 *   the country code, passed by reference
 */
function hook_uc_moneris_hpp_alter_country(&$country) {  
}

/*
 * Allows modules to alter the API fields that appear in the checkout
 * review form and that are posted to the Moneris HPP. 
 * 
 * See uc_moneris_hpp_form_alter() for the available fields.
 *
 * @param $vars
 *   array of API variables, in name => value format, passed by reference
 */
function hook_uc_moneris_hpp_formfields_alter(&$vars) {    
}

/*
 * Allows modules to alter the API fields that are used to verify the 
 * transaction. (This is probably necessary if you are also altering the
 * initial form fields above.)
 * 
 * See uc_moneris_hpp_return() for the available fields.
 *
 * @param $vars
 *   array of API variables, in name => value format, passed by reference
 */
function hook_uc_moneris_hpp_verify_alter(&$vars) {  
}
