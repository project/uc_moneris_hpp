Install this module as per usual. Enable the HPP option in Ubercart from 
Store administration > Configuration > Payment

To actually process CC payments through Moneris, you will need a Moneris 
merchant service account.

Follow the instructions at the beginning of the "Moneris Hosted Pay Page 
settings" section under Store administration > Configuration > Payment 
to set up your HPP account.

TESTING

To run test payments, set up an HPP store account on the Moneris test 
servers (they provide test logins to use):

Canada: https://esqa.moneris.com/mpg

USA: https://esplusqa.moneris.com/usmpg/

Once your HPP account is set up, you can test drive the HPP workflow in
Ubercart.

This module has two test modes: "test" and "debug". They differ in that 
"debug" prints a lot of messages to the screen for easier troubleshooting 
and development.
